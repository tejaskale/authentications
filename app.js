const express= require('express')
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken");

const app = express()

app.use(express.json())

const userData = []


//For login

app.post('/login',  (req,res)=>{
    
    const {username,password}=req.body
    const isUsernameValid = userData.find((user)=>user.username === username)
    const hashedPassword= isUsernameValid.password
    const isPasswordValid =  bcrypt.compare(password, hashedPassword);
    if(isUsernameValid === true && isPasswordValid === true)
    {
        res.status(200).send("Successfully logged in!!")
        console.log("Successfully logged in!!");
    }
    else{
        res.status(400).send("Invalid Credentials")
    }
} )




//for SignUP

app.post('/signup' , async (req , res)=>{
    
    
        const  {username,password}=req.body
        const hashedPassword = await bcrypt.hash(password, 10);
        const isAlreadyUser = userData.find(item=> item.username === req.body.username)
        if(!isAlreadyUser){
        userData.push({username,hashedPassword});
        console.log("New account successfully created");
        console.log(userData);
        }
        else {
            res.status(400).send("You are already a user, you can login")
        }
    
})



//For resetting password

app.patch("/reset", async(req, res) => {
      
    let authHeader = req.headers["authorization"]

    if(authHeader !== undefined){
       let jwtToken = authHeader.split(" ")[1]
       jwt.verify(jwtToken, "SECRET_KEY", async(error) => {
          if(error){
              res.send("Invalid access token")
          }else{
              const {username, oldPassword, newPassword} = req.body;
              const foundUser = userData.find(eachItem => eachItem.username === username);

              if(foundUser){
                  const {hashedPassword} = foundUser
                  const isPasswordMatched = await bcrypt.compare(oldPassword, hashedPassword)
          
                  if(isPasswordMatched){
                      const newHashedPassword = await bcrypt.hash(newPassword, 10);
                      userData.map(eachItem => {
                          if(eachItem.username === username){
                              eachItem["hashedPassword"] = newHashedPassword
                          }
                      })
                      
                      res.send("Password Updated Successfully")
                  }else{
                      res.send("Password is incorrect")
                  }
                }else{
                  res.send("user is inValid")
                }

          }
       })
    }else{
      res.send('required jwtToken')
    }
})




const PORT = process.env.PORT || 3000
app.listen(PORT,()=>console.log(`Server started listening on http://localhost:${PORT}`));
